import os # para interactuar con el OS
import logging # logs, mensajes y demás
import csv

import numpy as np
import cv2 # Open CV

import utils #diferentes utilidades de python


DIVIDER_COLOUR = (255, 255, 0)
BOUNDING_BOX_COLOUR = (7, 47, 95)
CENTROID_COLOUR = (100, 45, 55)
CAR_COLOURS = [(255, 45, 55)]
EXIT_COLOR = (255, 165, 0)


class PipelineExcec(object):
    '''
        # Pipeline: se pasan en cadena los datos a los 
        # diferetentes procesadores (En serie)

        # Se le pueden cambiar el nivel o modo de "logging:
        # a cada procesador
    '''

    ''' Constructor de Pipeline '''
    def __init__(self, pipeline=None, log_level=logging.DEBUG):
        self.pipeline = pipeline or []
        self.context = {}

        # Configuracion Logs
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.setLevel(log_level)
        self.log_level = log_level
        self.set_log_level()

    def set_context(self, data):
        self.context = data

    ''' Se agrega un procesador al pipeline'''
    def add(self, processor):
     
        processor.log.setLevel(self.log_level)
        self.pipeline.append(processor)

    def remove(self, name):
        for i, p in enumerate(self.pipeline):
            if p.__class__.__name__ == name:
                del self.pipeline[i]
                return True
        return False

    def set_log_level(self):
        for p in self.pipeline:
            p.log.setLevel(self.log_level)

    def run(self):
        for p in self.pipeline:
            self.context = p(self.context)

        self.log.debug("Frame ---> %d OK :)", self.context['frame_number'])

        return self.context


class Pipeline_Proccessor(object):
    '''
        Clase base para procesadores
    '''

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)


class ContourDetection(Pipeline_Proccessor):
    '''
        Detecting moving objects.

        1 - BG Subtraction
        2 - Detección de vehiculos con findContours de OpenCV
        3 - Eliminar algunos si se pasan de cierto height y width

        BackGrdSubtraction - background subtractor isinstance.
        min_contour_width - min bounding rectangle width.
        min_contour_height - min bounding rectangle height.
        save_image - if True will save detected objects mask to file.
        image_dir - where to save images(must exist).        
    '''

    def __init__(self, BackGrdSubtraction, min_contour_width=35, min_contour_height=35, save_image=False, image_dir='images'):
        super(ContourDetection, self).__init__()

        self.BackGrdSubtraction = BackGrdSubtraction
        self.min_contour_width = min_contour_width
        self.min_contour_height = min_contour_height
        self.save_image = save_image
        self.image_dir = image_dir

    def maskFiltering(self, img, a=None):
        '''
            Se utilizan los filtros:
            + Closing (quita gaps)
            + Opening (quitar ruido)
            + Dilation (se unen los blobs pegados)

        '''

        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))

        closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
        opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel)
        dilation = cv2.dilate(opening, kernel, iterations=2)

        return dilation

    def detect_vehicles(self, foregroundMask, context):

        # foregroundMask -> imagen que se le da a findContours

        #lista con centroides de vehiculos
        matches = []

        # CONTOURS:
        # # findContours Recibe:
        #     + Source = Imagen de input
        #     + Retrieval = el retrieval mode para sacar los contours
        #     + Aprox = modo de aproximación
        # En este caso usamos:
        #     Ret = cv2.CV_RETR_EXTERNAL ->saca solo los contours externos
        #     Aprox = cv2.CV_CHAIN_APPROX_TC89_L1 -> Teh-Chin chain approx, es mas rapida
        #  Outputs:
        #     + imagen modificada
        #     + contours (lista python con todos los contours de la imagen)
        #     + cada contour es un numpy array con las coordenadas de los puntos xy


        im2, contours, hierarchy = cv2.findContours(
            foregroundMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_L1)

        # Se recorren los contours y se "validan" si cumple con los maximos y minimos
        # de altura y ancho 
        for (i, contour) in enumerate(contours):

            # Se dibujan BOUNDING RECTANGLES

            (x, y, w, h) = cv2.boundingRect(contour)

            # Se verifica validez del contour segun H y W
            contour_valid = (w >= self.min_contour_width) and (
                h >= self.min_contour_height)

            if not contour_valid:
                # si no es valido se sigue con el siguiente contour
                continue


            # Se calcula en centroide con las dimensiones del bounding box
            centroid = utils.get_centroid(x, y, w, h)

            # Se agregan los centroides a la lista para devolverlos como resultado
            matches.append(((x, y, w, h), centroid))
        return matches

    def __call__(self, context):
        frame = context['frame'].copy()
        frame_number = context['frame_number']

        foregroundMask = self.BackGrdSubtraction.apply(frame, None, 0.001)
        # just thresholding values
        foregroundMask[foregroundMask < 240] = 0
        foregroundMask = self.maskFiltering(foregroundMask, frame_number)

        if self.save_image:
            utils.save_frame(foregroundMask, self.image_dir +
                             "/mask_%04d.png" % frame_number, flip=False)

        context['objects'] = self.detect_vehicles(foregroundMask, context)
        context['foregroundMask'] = foregroundMask

        return context


class Counter(Pipeline_Proccessor):
    '''
        # Contador de vehiculos que pasan la zona de salida

        # Se crean paths con los centroides de vehiculos detectados 
        # y se cuentan cuando entran a la zona de salida definida por
        # las exit masks

        # + ExitMasksList: lista con las exit maksks
        # + pathSize: max cantida de puntos en 1 path
        # + DistMaxPts - max distancia entre 2 puntos
    '''

    def __init__(self, ExitMasksList=[], pathSize=10, DistMaxPts=30, x_weight=1.0, y_weight=1.0):
        super(Counter, self).__init__()

        ''' Atributos '''
        self.ExitMasksList = ExitMasksList

        self.VehicleCounter = 0
        self.pathSize = pathSize
        self.paths = []
        self.DistMaxPts = DistMaxPts
        self.x_weight = x_weight
        self.y_weight = y_weight
    
    '''
    #   Se evaluan las coordenadas del punto
    #   en la exit mask para verificar si están en la mascara
    '''

    def checkExit(self, point):
        for exitMask in self.ExitMasksList:
            #print de prueba, quitar!
            #print(exitMask[point[1]][point[0]])
            try:
                if exitMask[point[1]][point[0]] == 255:
                    return True
            except:
                return True
        return False

    def __call__(self, context):
        objects = context['objects']
        context['ExitMasksList'] = self.ExitMasksList
        context['paths'] = self.paths
        context['VehicleCounter'] = self.VehicleCounter
        if not objects:
            return context

        points = np.array(objects)[:, 0:2]
        points = points.tolist()

        # append nuevos puntos si el path está vacío
        if not self.paths:
            for match in points:
                self.paths.append([match])

        else:
            '''
            # Se relacionan nuevos puntos a los caminos de acuerdo con la 
            # distancia minima entre puntos
            '''
            new_paths = []

            for path in self.paths:
                _min = 999999
                _match = None
                for p in points:

                    # Si solo hay 1 punto en el path:
                    if len(path) == 1:
                        # distancia entre el punto anterior y el actual
                        d = utils.distance(p[0], path[-1][0])
                    # Si ya hay mas de 2 puntos en el path
                    else:
                        #basandose en los 2 puntos anteriores predecir
                        #el siguiente punto y calcular la distancia predecida
                        #del siguiente punto al actual

                        #xnext y ynext
                        xn = 2 * path[-1][0][0] - path[-2][0][0]
                        yn = 2 * path[-1][0][1] - path[-2][0][1]

                        #distancia entre punto y punto next
                        d = utils.distance(
                            p[0], (xn, yn),
                            x_weight=self.x_weight,
                            y_weight=self.y_weight
                        )

                    if d < _min:
                        _min = d
                        _match = p

                if _match and _min <= self.DistMaxPts:
                    points.remove(_match)
                    path.append(_match)
                    new_paths.append(path)

                # no botar el path aunque no haya matches
                if _match is None:
                    new_paths.append(path)

            self.paths = new_paths

            # se agregan nuevos paths
            if len(points):
                for p in points:
                    # no se agregan puntos que ya deben ser contados

                    if self.checkExit(p[1]):
                        continue
                    self.paths.append([p])

        #Se guardan solo los ultimos N puntos en el path
        for i, _ in enumerate(self.paths):
            self.paths[i] = self.paths[i][self.pathSize * -1:]
        '''
        ######  CUENTA DE VEHICULOS !! #########
        # se cuentan los vehiculos y de descartan los paths
        # que ya se contaron
        '''
        new_paths = []

        #Se recorren los paths 
        for i, path in enumerate(self.paths):
            d = path[-2:]

            # VERIFICACIÓN PARA CONTAR
            if (
                # se necesitan al menos 2 puntos para contar
                len(d) >= 2 and
                # punto anterior NO exit zone
                not self.checkExit(d[0][1]) and
                # punto actual SI en exit zone
                self.checkExit(d[1][1]) and
                # path lenght es mayor al mínimo
                self.pathSize <= len(path)
            ):
                self.VehicleCounter += 1
            else:
                # prevenir uniones de puntos con paths que 
                # ya están en la exit zone
                add = True
                for p in path:
                    if self.checkExit(p[1]):
                        add = False
                        break
                if add:
                    new_paths.append(path)

        self.paths = new_paths

        context['paths'] = self.paths
        context['objects'] = objects
        context['VehicleCounter'] = self.VehicleCounter

        self.log.debug('Cantidad de Vehiculos ---> %s' % self.VehicleCounter)

        return context



class CsvWriter(Pipeline_Proccessor):

    def __init__(self, path, name, start_time=0, fps=15):
        super(CsvWriter, self).__init__()

        self.fp = open(os.path.join(path, name), 'w')
        self.writer = csv.DictWriter(self.fp, fieldnames=['tiempo (t)', 'Vehiculos'])
        self.writer.writeheader()
        self.start_time = start_time
        self.fps = fps
        self.path = path
        self.name = name
        self.prev = None

    def __call__(self, context):
        frame_number = context['frame_number']
        count = _count = context['VehicleCounter']

        if self.prev:
            _count = count - self.prev

        time = ((self.start_time + int(frame_number / self.fps)) * 100 
                + int(100.0 / self.fps) * (frame_number % self.fps))
        self.writer.writerow({'tiempo (t)': time, 'Vehiculos': _count})
        self.prev = count

        return context


class Visualizer(Pipeline_Proccessor):

    def __init__(self, save_image=True, image_dir='images'):
        super(Visualizer, self).__init__()

        self.save_image = save_image
        self.image_dir = image_dir

    def checkExit(self, point, ExitMasksList=[]):
        for exitMask in ExitMasksList:
            if exitMask[point[1]][point[0]] == 255:
                return True
        return False

    def draw_paths(self, img, paths):
        if not img.any():
            return

        for i, path in enumerate(paths):
            path = np.array(path)[:, 1].tolist()
            for point in path:
                cv2.circle(img, point, 2, CAR_COLOURS[0], -1)
                cv2.polylines(img, [np.int32(path)], False, CAR_COLOURS[0], 1)

        return img

    def draw_boxes(self, img, paths, ExitMasksList=[]):
        for (i, match) in enumerate(paths):

            contour, centroid = match[-1][:2]
            if self.checkExit(centroid, ExitMasksList):
                continue

            x, y, w, h = contour

            cv2.rectangle(img, (x, y), (x + w - 1, y + h - 1),
                          BOUNDING_BOX_COLOUR, 1)
            cv2.circle(img, centroid, 2, CENTROID_COLOUR, -1)

        return img

    def draw_ui(self, img, VehicleCounter, ExitMasksList=[]):

        # mascara de exit opaca para que se vea toda la imagen
        for exitMask in ExitMasksList:
            _img = np.zeros(img.shape, img.dtype)
            _img[:, :] = EXIT_COLOR
            mask = cv2.bitwise_and(_img, _img, mask=exitMask)
            cv2.addWeighted(mask, 1, img, 1, 0, img)

        return img

    def __call__(self, context):
        frame = context['frame'].copy()
        frame_number = context['frame_number']
        paths = context['paths']
        ExitMasksList = context['ExitMasksList']
        VehicleCounter = context['VehicleCounter']

        frame = self.draw_ui(frame, VehicleCounter, ExitMasksList)
        frame = self.draw_paths(frame, paths)
        frame = self.draw_boxes(frame, paths, ExitMasksList)

        utils.save_frame(frame, self.image_dir +
                         "/processed_%04d.png" % frame_number)

        return context
