import os # para interactuar con el OS 
import logging # logs, mensajes y demás
import logging.handlers 
import random
import subprocess # para calcular el tiempo (duración) del video

import numpy as np
import skvideo.io #read write videos
import cv2 # Open CV
import matplotlib.pyplot as plt

import utils #diferentes funciones que se necesitan

# resuelve problemas raros de opencv, no quitar
cv2.ocl.setUseOpenCL(False)
random.seed(123)

from pipeline import (
    PipelineExcec,
    ContourDetection,
    Visualizer,
    CsvWriter,
    Counter)

# =========================EDITAR DEPENDIENDO DEL ARCHIVO DE VIDEO DE ENTRADA==================================================
IMAGE_DIR = "./frames"
source = "traficCR2.mp4" # archivo de entrada
shape = (288, 616)  # Altura x Ancho video (HxW)
#LA ALTURA Y EL ANCHO SE PUEDE CONSEGUIR CON MÉTODOS DE OPENCV
#PARA ESTO REVISAR EL LINK :https://stackoverflow.com/questions/25359288/how-to-know-total-number-of-frame-in-a-file-with-cv2-in-python
#Exit points, lugares donde se cuentan los vehiculos detectados
ExitPoints = np.array([ 
    [[0, 288], [438, 288], [379, 220], [0, 220]]
    
])
# ==========================================================================================================================


#============Cálculo de tiempo del video ============

def get_length(filename):
    result = subprocess.run(["ffprobe", "-v", "error", "-show_entries",
                             "format=duration", "-of",
                             "default=noprint_wrappers=1:nokey=1", filename],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT)
    return float(result.stdout)


# Entrenamiento del método de bg sub, se le dan 500 frames para que corra el algoritmo 
# y de esta manera obtener un background definitivo y coherente.
# backsubMOG2 es el background subtractor createBackgroundSubtractorMOG2
# InputVideo es el video source
# num es la cantidad de frames
def train_BackGrdSubtraction(backsubMOG2, InputVideo, num=500): 
    # print ('Training BG Subtractor...')
    i = 0
    for frame in InputVideo:
        # se le da 1 frame a la función por ciclo
        backsubMOG2.apply(frame, None, 0.001)
        i = i + 1
        if i >= num:
            return InputVideo


def main():
    log = logging.getLogger("main")

    #Calculo de tiempo del video:
    video_length_secs = get_length(source)

    print (video_length_secs)

    single_frame_time = video_length_secs / (1308 / 2)

    

    # Se dibuja la máscara de exit points, en donde se cuentan los vehiculos
    # shape + (3,) = (H, W , 3)
    # base = base del tamaño del video 
    # fillPoly = recibe la base, los puntos del poligono, y color
    base = np.zeros(shape + (3,), dtype='uint8') 
    exitMask = cv2.fillPoly(base, ExitPoints, (255, 255, 255))[:, :, 0]


    # Llamada a funcion BackgroundSubtractorMOG2 de OpenCV con detección de sombras activado
    BackGrdSubtraction = cv2.createBackgroundSubtractorMOG2(
        history=500, detectShadows=True)

    # PIPELINE DE PROCESAMIENTO
    pipeline = PipelineExcec(pipeline=[
        ContourDetection(BackGrdSubtraction=BackGrdSubtraction,
                         save_image=True, image_dir=IMAGE_DIR),
        # y_weight == 2.0 because trafico vertical
        # usar x_weight == 2.0  para trafico horizontal.
        Counter(ExitMasksList=[exitMask], y_weight=2.0),
        Visualizer(image_dir=IMAGE_DIR),
        CsvWriter(path='./', name='datos.csv')
    ], log_level=logging.DEBUG)

    # Source de video, se llama con skvideo o con opencv
    InputVideo = skvideo.io.vreader(source)

    # Background Subtraction 
    train_BackGrdSubtraction(BackGrdSubtraction, InputVideo, num=500)

    # frame number real 
    _frame_number = -1
    # frame number cada dos frames que se le pasa al pipeline
    frame_number = -1

    #Contar cantidad total de frames: (poco elegante por el momento...)
    # total_frames = 0
    # for frame in InputVideo:
    #     total_frames = total_frames + 1

    # print("totalframessss", total_frames)
    #1308 total frames
    #REVISA POR QUE SE CIERRA EL PROGRAMA DESPUES DE ESTE FOR??????

    
    for frame in InputVideo:
       
        # frame number real
        _frame_number += 1

        # se trabaja cada 2 frames
        if _frame_number % 2 != 0:
            continue
       

        # frame number que se utiliza
        frame_number += 1

        # plt.imshow(frame)
        # plt.show()
        # return

        pipeline.set_context({
            'frame': frame,
            'frame_number': frame_number,
        })
        pipeline.run()

# ============================================================ #

if __name__ == "__main__":
    log = utils.init_logging()



    main()
